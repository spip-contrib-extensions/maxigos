<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/maxigos/trunk/lang/

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'maxigos_description' => 'maxigos propose un modèle pour Spip qui permet l\'implémentation des principales fonctionnalités de [maxiGos->http://jeudego.org/maxiGos/], « un ensemble d\'outils permettant d’insérer des parties, problèmes et diagrammes de go enregistrés au format sgf dans une page web. »

Le contenu sgf peut être écrit en paramètre du modèle ou contenu dans un fichier au format sgf ([Smart Game Format->https://en.wikipedia.org/wiki/Smart_Game_Format]).

Le modèle a été testé sous Spip 3.1, mais il n\'y a pas à priori de raison pour qu\'il ne fonctionne pas également sous Spip 3.0, ou 2.1 et 2.0. Simplement, il faut tester. Des retours d\'utilisateurs sont les bienvenus à ce propos.',
	'maxigos_nom' => 'Intégration maxiGos',
	'maxigos_slogan' => 'Diagrammes de go avec maxiGos V6.4.'

);
?>
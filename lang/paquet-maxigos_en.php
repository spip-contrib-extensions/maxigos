<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/maxigos/trunk/lang/

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'maxigos_description' => 'maxigos proposes a model for Spip which allows the implementation of the main functionalities of [maxiGos-> http://jeudego.org/maxiGos/], « a set of tools allowing to insert parts, problems and diagrams of Go saved in sgf format in a web page. »

The sgf content can be written as a model parameter or contained in a sgf file ([Smart Game Format-> https://en.wikipedia.org/wiki/Smart_Game_Format]).

The model has been tested under Spip 3.1, but there is not particular reason that it does not work also under Spip 3.0, or 2.1 and 2.0. In other words, you have to test. Feedback from users is welcome.',
	'maxigos_nom' => 'maxiGos integration',
	'maxigos_slogan' => 'Go diagrams with maxiGos V6.4.'

);
?>